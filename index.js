/**
 * Name: Yash Khanchandani
 * Date: July 28, 2020
 * Section: CSE 154 AC
 * This is the JS to implement the UI for my cryptogram generator, and generate
 * different types of ciphers from user input.
 */
/**
 * Global Varibles
 */
"use strict";
let pokeAPI = "https://pokeapi.co/api/v2/pokemon/";

/**
 * This is a window functions allows the buttons to be clicked.
 */
(function() {
  window.addEventListener('load', init);

  /**
   * this is the init function
   */
  function init() {
    let searchbutt = document.getElementById("pokedex-button");
    if (!(searchbutt === null)) {
      searchbutt.addEventListener("click", function(event) {
        event.preventDefault();
        emptyString();
      });
    }
  }

  /**
   * this function check if the string is empty then it does nothing
   * otherwise it calls findpokemon()
   */
  function emptyString() {
    let pokeIdentity = document.getElementById("pokeinput").value;
    if (!(pokeIdentity === "" || pokeIdentity === null)) {
      findPokemon();
    }
  }

  /**
   * this call the fetch command this does the ajax
   */
  function findPokemon() {
    let pokeIdentity = document.getElementById("pokeinput").value;
    let url = pokeAPI + pokeIdentity;
    fetch(url)
      .then(checkstatus)
      .then(res => res.json())
      .then(processResponse)
      .catch(handleError);
  }

  /**
   * this check if it is ok
   * @param {Object} res this is the url to check
   * @returns {Object} this is the url
   */
  function checkstatus(res) {
    if (res.ok) {
      return res;
    } else {
      throw Error(res.status + " in Request " + res.statusText);
    }
  }

  /**
   * this take the json and make many different varibles for the pokemon card
   * this constructs the card to show when the the pokemon is there
   * @param {Object} data json of the pokemon url
   * @returns {Object} return data object
   */
  function processResponse(data) {
    if (document.getElementById("pokemon-card").classList.contains("hidden")) {
      document.getElementById("pokemon-error").classList.toggle("hidden");
      document.getElementById("pokemon-card").classList.toggle("hidden");
      document.getElementById("poke-err").innerText = "";
    }
    let pokemon = {};
    pokemon["name"] = data.name;
    pokemon["id"] = data.id;
    pokemon["image1"] = data.sprites["front_default"];
    pokemon["type"] = data.types[0].type.name;
    if (data.types.length === 2) {
      pokemon["type"] = data.types[0].type.name + ", " + data.types[1].type.name;
    }
    let nameandId = document.getElementById("pokemonsad");
    nameandId.innerText = pokemon["name"] + " - " + pokemon["id"];
    let var1 = document.getElementById("pokemon-card");
    addImgDivs(var1, pokemon);
    let pdiv;
    if (!(document.getElementById("poke-para") === null)) {
      pdiv = document.getElementById("poke-para");
      pdiv.innerText = "Type(s): " + pokemon["type"];
    } else {
      pdiv = document.createElement("p");
      pdiv.id = "poke-para";
      pdiv.innerText = "Type(s): " + pokemon["type"];
      var1.appendChild(pdiv);
    }
    return data;
  }

  /**
   * this add the imgDiv for the card
   * @param {DomElement} var1 this is the card itself
   * @param {Object} pokemon this is the structure that contains pokemon details
   */
  function addImgDivs(var1, pokemon) {
    let imgDiv;
    if (!(document.getElementById("poke-img") === null)) {
      imgDiv = document.getElementById("poke-img");
      imgDiv.src = pokemon["image1"];
      imgDiv.alt = pokemon["name"];
    } else {
      imgDiv = document.createElement("img");
      imgDiv.id = "poke-img";
      imgDiv.src = pokemon["image1"];
      imgDiv.alt = pokemon["name"];
      var1.appendChild(imgDiv);
    }
  }

  /**
   * This function hides the pokemon card and show the error message
   * @param {String} err This is the string of the error
   * @returns {String} err is the string
   */
  function handleError(err) {
    let pokeErr = document.getElementById("poke-err");
    pokeErr.innerText = err;
    if (document.getElementById("pokemon-error").classList.contains("hidden")) {
      document.getElementById("pokemon-card").classList.toggle("hidden");
      document.getElementById("pokemon-error").classList.toggle("hidden");
    }
    return err;
  }
})();